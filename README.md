# Reverse Proxy for Docker built on Traefik, nginx and acme.sh

Unfortunately, Traefik is just a little too unflexible for HSTS, redirects, error pages and some scenarios for certificate acquisition.  
For that reason (and because we wanted a KISSful system with a project-based folder structure for farafin.de), I've built this rock-solid deployment of [Traefik 2](https://docs.traefik.io/v2.0/) (as a reverse proxy to Docker containers), [nginx](https://www.nginx.com/) (for TLS termination and error pages) and [acme.sh](https://acme.sh) (for certificate acquisition).

**The use case:** you have one or more domains, you want a wildcard certificate for them, and you want to be able to run any Docker container on any subdomain of your choice.  
If you don't want a wildcard or DNS-based certificates, this will require some changes and you might be better off just configuring Traefik yourself as a standalone service.

**To get started, just run the following commands:**

```bash
# clone the repository (use a directory of your choice)
git clone https://codeberg.org/momar/reverse-proxy-for-docker.git /data/proxy && cd /data/proxy

# generate strong diffie-hellman parameters for nginx
openssl dhparam -out nginx-dhparams.pem 4096

# adjust domains and certificate paths - you can search for "farafin.de" to find parts to replace
vim docker-compose.yml
vim nginx.conf

# specify domains, containers to automatically restart, and the hosting.de API key:
docker-compose run -e HOSTINGDE_APIKEY='...' acme --issue \
  --renew-hook 'apk add --no-cache docker-cli && { docker restart proxy_nginx_1 ldap_slapd_1 || true; }' \
  --dns dns_hostingde -d 'farafin.de' -d '*.farafin.de'

# restrict file permissions
sudo sh -c 'chown root:root nginx-dhparams.pem && chmod go-rwx nginx-dhparams.pem certificates'

# start everything up
docker-compose up -d
```

To publish a Docker container under a specific domain, use `--label traefik.domain=whatever.example.org`. This enables Traefik and sets the default domain. Other Traefik options can still be used, e.g. `--label traefik.http.services.default.loadbalancer.server.port=8080` or `--label traefik.http.routers.default.priority=...`.
